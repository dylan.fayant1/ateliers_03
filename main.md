# Main App

Voici le fichier `readme.md` de ma branche principale. 

A ce stade. Je n'ai que la branche main et que ce fichier là.

# Feature 01 

Si tu es sur la branche `main` et que tu vois ce fichier dans le repository, alors c'est que tu as réussi ton merge de ta branche feature1 vers la main.
Si tu ne le vois pas. Alors le merge n'a pas encore été fait.

J'ajoute cette ligne pour provoquer une désynchronisation de repository entre gitlab et mon poste.